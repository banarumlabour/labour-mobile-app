package com.banarum.labour.social

import android.app.Activity
import android.app.Fragment
import android.content.Context
import android.os.Bundle
import com.facebook.*
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import org.json.JSONObject
import java.util.*
import javax.inject.Inject

class FacebookApi {

    lateinit var loginManager: LoginManager
    lateinit var callbackManager: CallbackManager

    lateinit var onSuccess: ((String, String, AccessToken) -> Unit)
    lateinit var onResponseSent: (() -> Unit)
    lateinit var onCancel: (() -> Unit)
    lateinit var onError: ((FacebookException) -> Unit)
    lateinit var onResponseError: ((Exception) -> Unit)

    var callback: (JSONObject?, GraphResponse?) -> Unit = { obj, response ->
        try {
            val id = obj?.getString("id")
            val name = obj?.getString("name")
            val token = AccessToken.getCurrentAccessToken()
            if (id != null && name != null)
                this@FacebookApi.onSuccess(id, name, token)
            else
                throw Exception("Response Error")

        } catch (e: Exception) {
            e.printStackTrace()
            this@FacebookApi.onResponseError(e)
        }
    }

    fun init() {
        callbackManager = CallbackManager.Factory.create()
        loginManager = LoginManager.getInstance()
        loginManager.registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
            override fun onSuccess(loginResult: LoginResult) {
                val request = GraphRequest.newMeRequest(loginResult.accessToken, callback)
                val parameters = Bundle()
                parameters.putString("fields", "id,name,email")
                request.parameters = parameters
                request.executeAsync()
                onResponseSent()
            }

            override fun onCancel() = this@FacebookApi.onCancel()

            override fun onError(error: FacebookException) = this@FacebookApi.onError(error)
        })
    }

    fun tryColdAuth(): Boolean {
        val token: AccessToken? = AccessToken.getCurrentAccessToken()

        if (token != null && !token.isExpired) {
            val request = GraphRequest.newMeRequest(token, callback)
            val parameters = Bundle()
            parameters.putString("fields", "id,name,email")
            request.parameters = parameters
            request.executeAsync()
            return true
        }
        return false
    }

    fun logOut() = loginManager.logOut()

    fun logIn(activity: Activity) = loginManager.logInWithReadPermissions(activity, Arrays.asList("public_profile"))

    fun logIn(fragment: Fragment) = loginManager.logInWithReadPermissions(fragment, Arrays.asList("public_profile"))
}