package com.banarum.labour.social

import android.app.Activity
import android.app.Fragment
import android.content.Intent
import com.vk.sdk.VKAccessToken
import com.vk.sdk.VKCallback
import com.vk.sdk.VKSdk
import com.vk.sdk.api.VKApi
import com.vk.sdk.api.VKError
import com.vk.sdk.api.VKRequest
import com.vk.sdk.api.VKResponse

class VkApi {

    lateinit var onSuccess: ((String, String, VKAccessToken) -> Unit)
    lateinit var onResponseSent: (() -> Unit)
    lateinit var onError: ((VKError) -> Unit)
    lateinit var onResponseError: ((Exception) -> Unit)

    var callback: VKRequest.VKRequestListener = object : VKRequest.VKRequestListener() {
        override fun onComplete(response: VKResponse) {
            try {
                val jsonResponse = response.json.getJSONArray("response").getJSONObject(0)
                val id = jsonResponse.getString("id")
                val name = jsonResponse.getString("first_name") + " " + jsonResponse.getString("last_name")
                val token = VKAccessToken.currentToken()
                if (id != null)
                    this@VkApi.onSuccess(id, name, token)
                else
                    throw Exception("Response Error")
            } catch (e: Exception) {
                e.printStackTrace()
                this@VkApi.onResponseError(e)
            }
        }

        override fun onError(error: VKError) = this@VkApi.onError(error)
    }

    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?): Boolean {
        return VKSdk.onActivityResult(requestCode, resultCode, data, object : VKCallback<VKAccessToken> {
            override fun onResult(res: VKAccessToken?) {
                val request = VKApi.users().get()
                request.executeWithListener(callback)
                onResponseSent()
            }

            override fun onError(error: VKError) = this@VkApi.onError(error)
        })
    }

    fun tryColdAuth(): Boolean {
        val token: VKAccessToken? = VKAccessToken.currentToken()

        if (token != null && !token.isExpired) {
            val request = VKApi.users().get()
            request.executeWithListener(callback)
            return true
        }
        return false
    }

    fun logOut() = VKSdk.logout()

    fun logIn(activity: Activity) = VKSdk.login(activity)

    fun logIn(fragment: Fragment) = VKSdk.login(fragment)
}