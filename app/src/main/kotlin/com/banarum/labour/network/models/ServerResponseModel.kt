package com.banarum.labour.network.models

data class ServerResponseModel<T>(val status:Int,
                          val cause:String?,
                          val msg:String,
                          val data:T)