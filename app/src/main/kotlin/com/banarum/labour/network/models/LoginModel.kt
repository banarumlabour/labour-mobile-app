package com.banarum.labour.network.models

import paperparcel.PaperParcel
import paperparcel.PaperParcelable

@PaperParcel
data class LoginModel(val loginType:String,
                      val id:String,
                      val name:String,
                      val token:String) : PaperParcelable {
    companion object {
        @JvmField val CREATOR = PaperParcelLoginModel.CREATOR
    }
}