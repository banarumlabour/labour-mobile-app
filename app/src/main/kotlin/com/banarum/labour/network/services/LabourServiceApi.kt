package com.banarum.labour.network.services

import com.banarum.labour.network.models.LabeModel
import com.banarum.labour.network.models.ServerResponseModel
import com.banarum.labour.network.models.UserModel
import retrofit2.http.GET
import retrofit2.http.Query
import rx.Observable

interface LabourServiceApi {
    @GET("login.php")
    fun getUserData(
            @Query("user_platform") platform: String,
            @Query("user_id") userId: String,
            @Query("user_name") name: String
    ): Observable<ServerResponseModel<UserModel>?>

    @GET("labes.php")
    fun getLabesData(@Query("category") category: String?): Observable<ServerResponseModel<List<LabeModel>>?>

    @GET("users.php")
    fun getUserData(@Query("local_id") id: Long): Observable<ServerResponseModel<UserModel>?>

    @GET("add_labe.php")
    fun postLabe(@Query("id") id: Long,
                 @Query("title") title: String,
                 @Query("description") description: String,
                 @Query("price") price: Int,
                 @Query("is_hot") isHot: Int):Observable<Void>
}