package com.banarum.labour.network.models

import paperparcel.PaperParcel
import paperparcel.PaperParcelable

@PaperParcel
data class UserModel(
        val id:Long,
        val platform:String,
        val userId:String,
        val name:String,
        val icoUrl:String,
        val rating:Int,
        val labes:Int) : PaperParcelable {
    companion object {
        @JvmField val CREATOR = PaperParcelUserModel.CREATOR
    }
}