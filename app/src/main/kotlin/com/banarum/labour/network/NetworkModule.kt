package com.banarum.labour.network

import com.banarum.labour.BuildConfig
import com.banarum.labour.network.services.LabourService
import com.banarum.labour.network.services.LabourServiceApi
import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import rx.schedulers.Schedulers
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

private const val DEFAULT_CONNECT_TIMEOUT = 60000L
private val HTTP_LOG_LEVEL = if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.BASIC
private const val LABOUR_BASE_URL = "http://lion.pe.hu/mobileapi/"

@Module
class NetworkModule{

    @Provides
    @Singleton
    fun provideOkHttpClient(): OkHttpClient {
        return OkHttpClient.Builder()
                .connectTimeout(DEFAULT_CONNECT_TIMEOUT, TimeUnit.MILLISECONDS)
                .readTimeout(DEFAULT_CONNECT_TIMEOUT, TimeUnit.MILLISECONDS)
                .writeTimeout(DEFAULT_CONNECT_TIMEOUT, TimeUnit.MILLISECONDS)
                .addInterceptor(HttpLoggingInterceptor().apply { level = HTTP_LOG_LEVEL })
                .build()
    }

    @Provides
    @Singleton
    fun provideDefaultMoshi(): Moshi {
        return  Moshi.Builder()
                //.add(DateAdapter())
                .build()
    }

    @Provides
    @Singleton
    fun labourService(httpClient: OkHttpClient, moshi: Moshi): LabourService {
        val retrofit = Retrofit.Builder()
                .addConverterFactory(MoshiConverterFactory.create(moshi))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.createWithScheduler(Schedulers.io()))
                .client(httpClient)
                .baseUrl(LABOUR_BASE_URL)
                .build()

        val labourServiceApi = retrofit.create(LabourServiceApi::class.java)

        return LabourService(labourServiceApi)
    }
}