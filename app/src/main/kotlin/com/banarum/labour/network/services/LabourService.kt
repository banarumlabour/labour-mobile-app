package com.banarum.labour.network.services

import com.banarum.labour.network.models.LabeModel
import com.banarum.labour.network.models.LoginModel
import com.banarum.labour.network.models.ServerResponseModel
import com.banarum.labour.network.models.UserModel
import rx.Observable


class LabourService(private val service: LabourServiceApi) {
    fun getUserData(loginModel:LoginModel): Observable<ServerResponseModel<UserModel>?> {
        return service.getUserData(loginModel.loginType, loginModel.id, loginModel.name)
    }

    fun getLabesData(category: String?): Observable<ServerResponseModel<List<LabeModel>>?> {
        return service.getLabesData(category)
    }

    fun getUserData(id: Long): Observable<ServerResponseModel<UserModel>?>{
        return service.getUserData(id)
    }

    fun postLabe(id: Long, title: String, description: String, price: Int, isHot: Boolean) : Observable<Void>{
        return service.postLabe(id, title, description, price, if (isHot) 1 else 0)
    }
}