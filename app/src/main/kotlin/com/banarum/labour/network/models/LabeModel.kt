package com.banarum.labour.network.models

import paperparcel.PaperParcel
import paperparcel.PaperParcelable

@PaperParcel
data class LabeModel(
        val title:String,
        val description:String,
        val date:String,
        val price:Long,
        val currency:String,
        val userName:String,
        val userId:Long,
        val contactInfo:String,
        val isContactInfoAllowed:Boolean,
        val isHot:Boolean) : PaperParcelable {
    companion object {
        @JvmField val CREATOR = PaperParcelLabeModel.CREATOR
    }
}