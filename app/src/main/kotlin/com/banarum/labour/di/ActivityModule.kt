package com.banarum.labour.di

import android.app.Activity
import android.content.Context
import com.banarum.labour.di.scope.ActivityScope
import dagger.Module
import dagger.Provides

@Module
abstract class ActivityModule(protected val activity: Activity) {

    @Provides @ActivityScope
    fun provideActivity(): Activity = activity

    @Provides @ActivityScope
    fun provideActivityContext(): Context = activity.baseContext
}