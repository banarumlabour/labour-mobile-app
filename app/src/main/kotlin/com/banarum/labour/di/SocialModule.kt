package com.banarum.labour.di

import com.banarum.labour.di.scope.ActivityScope
import com.banarum.labour.social.FacebookApi
import com.banarum.labour.social.VkApi
import com.facebook.login.LoginManager
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class SocialModule {

    @Provides
    @Singleton
    fun provideFacebookApi(): FacebookApi = FacebookApi()

    @Provides
    @Singleton
    fun provideVkApi(): VkApi = VkApi()
}