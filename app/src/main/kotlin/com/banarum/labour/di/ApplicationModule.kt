package com.banarum.labour.di

import android.content.Context
import com.banarum.labour.app.LabourApp
import com.banarum.labour.di.qualifier.ApplicationQualifier
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ApplicationModule(private val app: LabourApp) {

    @Provides @Singleton
    fun provideApplication(): LabourApp = app

    @Provides @Singleton @ApplicationQualifier
    fun provideApplicationContext(): Context = app
}