package com.banarum.labour.di

import android.support.v7.view.menu.MenuPresenter
import com.banarum.labour.di.subcomponent.LoginActivityComponent
import com.banarum.labour.di.subcomponent.LoginActivityModule
import com.banarum.labour.ui.login.LoginPresenter
import dagger.Component
import javax.inject.Singleton
import com.banarum.labour.network.NetworkModule
import com.banarum.labour.ui.card.CardPresenter
import com.banarum.labour.ui.menu.fragments.HomeView.AllLabesView.AllLabesPresenter
import com.banarum.labour.ui.menu.fragments.HomeView.HomePresenter
import com.banarum.labour.ui.menu.fragments.HomeView.MyLabesView.MyLabesPresenter
import com.banarum.labour.ui.menu.fragments.HotView.HotPresenter
import com.banarum.labour.ui.menu.fragments.ProfileView.ProfilePresenter
import com.banarum.labour.ui.post.PostPresenter

@Singleton
@Component(modules = arrayOf(
        ApplicationModule::class,
        SocialModule::class,
        NetworkModule::class
))
interface ApplicationComponent {

    fun plus(module: LoginActivityModule): LoginActivityComponent
    fun inject(presenter: LoginPresenter)
    fun inject(presenter: ProfilePresenter)
    fun inject(presenter: HomePresenter)
    fun inject(presenter: HotPresenter)
    fun inject(presenter: CardPresenter)
    fun inject(presenter: AllLabesPresenter)
    fun inject(presenter: MyLabesPresenter)
    fun inject(presenter: PostPresenter)
}