package com.banarum.labour.di.qualifier

import javax.inject.Qualifier

@Qualifier
annotation class ApplicationQualifier