package com.banarum.labour.di.subcomponent

import com.banarum.labour.di.ActivityModule
import com.banarum.labour.ui.login.LoginActivity
import dagger.Module

@Module
class LoginActivityModule(activity: LoginActivity) : ActivityModule(activity) {

}