package com.banarum.labour.di.subcomponent

import com.banarum.labour.di.scope.ActivityScope
import com.banarum.labour.ui.login.LoginActivity
import dagger.Subcomponent

@ActivityScope
@Subcomponent(modules = arrayOf(
        LoginActivityModule::class
))
interface LoginActivityComponent {
    fun inject(activity: LoginActivity)
}