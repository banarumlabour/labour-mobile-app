package com.banarum.labour.ui.menu.fragments.ProfileView

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import butterknife.ButterKnife
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.banarum.labour.R
import com.banarum.labour.common.transforms.CircleTransform
import com.banarum.labour.network.models.LoginModel
import com.banarum.labour.network.models.UserModel
import com.banarum.labour.ui.login.LoginActivity
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_profile.*
import org.jetbrains.anko.onClick

class ProfileFragment : MvpAppCompatFragment(), ProfileView {

    @InjectPresenter
    lateinit var presenter: ProfilePresenter

    companion object {
        @JvmField val PRM_ACTION_LOGOUT = "LogOut"
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        super.onCreate(savedInstanceState)
        val view = inflater!!.inflate(R.layout.fragment_profile, container, false);
        ButterKnife.bind(this, view)
        return view
    }

    override fun onStart() {
        super.onStart()


        val userModel: UserModel = activity.intent.getParcelableExtra(LoginActivity.PRM_USER_MODEL)

        presenter.onUserModel(userModel)

        profile_logout_button.onClick {
            onLogOut()
        }
    }

    override fun setUserData(userModel: UserModel) {
        profile_user_name.text = userModel.name
        setProfilePhoto(userModel.icoUrl)
        profile_labes.text = "${userModel.labes} labes"

    }

    fun onLogOut() {
        presenter.logOut()
        val intent = Intent(this.activity, LoginActivity::class.java)
        intent.putExtra(PRM_ACTION_LOGOUT, true)
        this.activity.startActivity(intent)
    }

    fun setProfilePhoto(url: String) {
        Picasso.with(this.activity)
                .load(url)
                .centerCrop()
                .transform(CircleTransform())
                .placeholder(R.drawable.profile_photo_placeholder)
                .fit()
                .into(profile_user_icon)
    }

    /*fun setRating (rating : String) {
        var newRating = rating.toFloat()
        for (i in 5 downTo 1) {
            if (newRating >= 1)
        }
    }*/

}