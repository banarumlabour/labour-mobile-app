package com.banarum.labour.ui.menu.fragments.HotView

import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import butterknife.ButterKnife
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.banarum.labour.R
import com.banarum.labour.network.models.LabeModel
import com.banarum.labour.network.models.UserModel
import com.banarum.labour.ui.card.CardActivity
import kotlinx.android.synthetic.main.fragment_hot.*

class HotFragment : MvpAppCompatFragment(), HotView {

    @InjectPresenter
    lateinit var presenter: HotPresenter

    companion object {
        @JvmField val CARD_ACTIVITY = "labe"
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        super.onCreate(savedInstanceState)
        val view = inflater!!.inflate(R.layout.fragment_hot, container, false);
        return view
    }

    override fun onStart() {
        super.onStart()
        hot_refresh_layout.setOnRefreshListener {
            onRefresh()
        }
        presenter.refreshLabes(null)

    }

    fun onRefresh(){
        presenter.refreshLabes(null)
    }

    override fun onDataList(data: List<LabeModel>) {
        val labesAdapter = HotLabesAdapter()
        labesAdapter.data = data

        labesAdapter.setLabeClickAction { item ->
            //TODO действие по клику
            val intent = Intent(this.activity, CardActivity::class.java)
            intent.putExtra(CARD_ACTIVITY, item)
            this.activity.startActivity(intent)
        }

        hot_recycler_view.layoutManager = LinearLayoutManager(this.activity!!)
        hot_recycler_view.adapter = labesAdapter

        hot_refresh_layout.isRefreshing = false
    }
}