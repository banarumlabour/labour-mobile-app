package com.banarum.labour.ui.card

import android.util.Log
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.banarum.labour.app.LabourApp
import com.banarum.labour.network.models.UserModel
import com.banarum.labour.network.services.LabourService
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import javax.inject.Inject

/**
 * Created by 1 on 14.03.2017.
 */

@InjectViewState
class CardPresenter : MvpPresenter<CardView> {

    @Inject
    lateinit var labourService:LabourService

    var userModel:UserModel? = null

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
    }

    constructor() : super() {
        LabourApp.graph.inject(this)
    }

    fun getAuthorData(id:Long){
        val request = labourService.getUserData(id)
        request.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map{ responseModel -> responseModel?.data}
                .onErrorReturn { null }
                .doOnError{

                }
                .subscribe{userModel -> viewState.onUserModel(userModel!!)}
    }

}