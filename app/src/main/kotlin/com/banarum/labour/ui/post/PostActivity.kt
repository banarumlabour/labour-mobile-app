package com.banarum.labour.ui.post

import android.os.Bundle
import android.os.PersistableBundle
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.banarum.labour.R
import com.banarum.labour.network.models.LoginModel
import com.banarum.labour.network.models.UserModel
import com.banarum.labour.ui.login.LoginActivity
import kotlinx.android.synthetic.main.activity_labe_add.*
import org.jetbrains.anko.onClick

class PostActivity : MvpAppCompatActivity(), PostView {

    @InjectPresenter
    lateinit var presenter:PostPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_labe_add)
        setSupportActionBar(my_toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        val userModel = intent.getParcelableExtra<UserModel>(LoginActivity.PRM_USER_MODEL)

        send_btn.onClick {
            if (title_txt.length()>3 && description_txt.length()>3 && price_txt.length()>0) {
                presenter.sendLabe(userModel, title_txt.text.toString(), description_txt.text.toString(), price_txt.text.toString().toInt(), true)
                showToast(getString(R.string.labe_posted))
                this.finish()
            }
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return true
    }

    fun showToast(msg: String?) {
        if (msg!=null)
            Toast.makeText(this, msg, Toast.LENGTH_LONG).show()
    }
}
