package com.banarum.labour.ui.menu.fragments.HomeView

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import com.banarum.labour.ui.menu.fragments.HomeView.AllLabesView.AllLabesFragment
import com.banarum.labour.ui.menu.fragments.HomeView.MyLabesView.MyLabesFragment

class ViewPagerAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {
    @JvmField val NUM_PAGES = 2

    override fun getItem(position: Int): Fragment {
        when (position){
            0 -> return AllLabesFragment()
            1 -> return MyLabesFragment()
        }
        return AllLabesFragment()
    }

    override fun getCount(): Int {
        return NUM_PAGES
    }
}