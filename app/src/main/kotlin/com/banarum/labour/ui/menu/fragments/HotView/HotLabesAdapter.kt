package com.banarum.labour.ui.menu.fragments.HotView

import android.view.View
import android.widget.TextView
import com.banarum.labour.R
import com.banarum.labour.network.models.LabeModel
import ru.appkode.baseui.recycler_view.adapters.ListAdapter

class HotLabesAdapter  : ListAdapter<LabeModel, HotLabesAdapter.ViewHolder>(R.layout.hot_list_item) {

    private var labeClickAction: ((LabeModel) -> Unit)? = null

    override fun createViewHolder(itemView: View): ViewHolder {
        return ViewHolder(itemView)
    }

    override fun bindViewHolder(holder: ViewHolder, item: LabeModel) {
        holder.titleText.text = item.title
        holder.descriptionText.text = item.description
        //holder.dateText.text = item.date

        when(item.currency) {
            "RUB" -> holder.priceText.text = (item.price/1000000).toString() + "₽"
        }
    }

    inner class ViewHolder(val view: View) : ListAdapter.ViewHolder(view) {
        override val clickTarget = null

        val titleText = view.findViewById(R.id.hot_title_text_item) as TextView
        val descriptionText = view.findViewById(R.id.hot_description_text_item) as TextView
        val priceText = view.findViewById(R.id.hot_cash_text_item) as TextView
        //val dateText = view.findViewById(R.id.hot_date_text_item) as TextView

        init {
            if (labeClickAction != null) {
                view.setOnClickListener {
                    labeClickAction?.invoke(
                            getItem(adapterPosition)
                    )
                }
            }
        }
    }

    fun setLabeClickAction(action: (LabeModel) -> Unit) {
        labeClickAction = action
    }
}