package com.banarum.labour.ui.card

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.banarum.labour.network.models.UserModel

/**
 * Created by 1 on 14.03.2017.
 */


@StateStrategyType(value = AddToEndSingleStrategy::class)
interface CardView : MvpView {
    fun onUserModel(userModel: UserModel)

 

}