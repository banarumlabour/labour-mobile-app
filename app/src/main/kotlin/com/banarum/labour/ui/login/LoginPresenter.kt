package com.banarum.labour.ui.login

import android.app.Activity
import android.app.Fragment
import android.content.Intent
import android.util.Log
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.banarum.labour.app.LabourApp
import com.banarum.labour.network.models.LoginModel
import com.banarum.labour.network.models.UserModel
import com.banarum.labour.network.services.LabourService
import com.banarum.labour.social.FacebookApi
import com.banarum.labour.social.VkApi
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import javax.inject.Inject

@InjectViewState
class LoginPresenter : MvpPresenter<LoginView>() {

    @Inject
    lateinit var labourService:LabourService

    companion object {
        @JvmField val FB_LOGIN_TYPE = "fb"
        @JvmField val VK_LOGIN_TYPE = "vk"
    }

    lateinit var facebookApi: FacebookApi
    lateinit var vkApi: VkApi

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        LabourApp.graph.inject(this)
    }

    fun facebookLogin(activity: Activity) {
        facebookApi.logIn(activity)
    }

    fun facebookLogin(fragment: Fragment) {
        facebookApi.logIn(fragment)
    }

    fun vkLogin(activity: Activity) {
        vkApi.logIn(activity)
    }

    fun vkLogin(fragment: Fragment) {
        vkApi.logIn(fragment)
    }

    @Inject
    fun initFacebookApi(facebookApi: FacebookApi) {
        this.facebookApi = facebookApi
        facebookApi.onResponseError = { jsonEx ->
            viewState.showToast(jsonEx.localizedMessage)
            viewState.showContentView()
            facebookApi.logOut()
        }
        facebookApi.onError = { fbEx ->
            viewState.showToast(fbEx.localizedMessage)
            viewState.showContentView()
        }
        facebookApi.onCancel = { viewState.showContentView() }
        facebookApi.onResponseSent = { viewState.showLoadingView() }

        facebookApi.onSuccess = { id, name, accessToken ->
            val loginModel = LoginModel(
                    FB_LOGIN_TYPE,
                    id,
                    name,
                    accessToken.token
            )
            onLoginSuccess(loginModel)
        }
        facebookApi.init()
        val success = facebookApi.tryColdAuth()
        if (success)
            viewState.showLoadingView()
    }

    @Inject
    fun initVkApi(vkApi: VkApi) {
        this.vkApi = vkApi
        vkApi.onResponseError = { jsonEx ->
            viewState.showToast(jsonEx.localizedMessage)
            viewState.showContentView()
            vkApi.logOut()
        }
        vkApi.onError = { vkEx ->
            viewState.showToast(vkEx.errorMessage)
            viewState.showContentView()
        }
        vkApi.onResponseSent = { viewState.showLoadingView() }

        vkApi.onSuccess = { id, name, accessToken ->
            val loginModel = LoginModel(
                    VK_LOGIN_TYPE,
                    id,
                    name,
                    accessToken.accessToken
            )
            onLoginSuccess(loginModel)
        }

        val success = vkApi.tryColdAuth()
        if (success)
            viewState.showLoadingView()
    }

    private fun onUserModel(userModel:UserModel){
        Log.d("kek_tag", userModel.icoUrl)
        viewState.onLoginSucceed(userModel)
    }

    private fun requestUserModel(loginModel:LoginModel){
        val request = labourService.getUserData(loginModel)
        request.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map{ responseModel -> responseModel?.data}
                .onErrorReturn { null }
                .doOnError{
                    viewState.showContentView()
                    viewState.showToast("Error")
                }
                .subscribe{userModel -> onUserModel(userModel!!)}
    }

    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        facebookApi.callbackManager.onActivityResult(requestCode, resultCode, data)
        vkApi.onActivityResult(requestCode, resultCode, data)
    }

    private fun onLoginSuccess(loginModel: LoginModel) {
        Log.d("LoginPresenter", "Welcome: ${loginModel.name} id: ${loginModel.id} through: ${loginModel.loginType}")
        requestUserModel(loginModel)
    }
}