package com.banarum.labour.ui.menu.fragments.HomeView

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.banarum.labour.R
import com.banarum.labour.network.models.LabeModel
import com.banarum.labour.ui.menu.fragments.HotView.HotLabesAdapter
import kotlinx.android.synthetic.main.fragment_home.*

class HomeFragment : MvpAppCompatFragment(), HomeView {

    @InjectPresenter
    lateinit var presenter: HomePresenter

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        super.onCreate(savedInstanceState)
        val view = inflater!!.inflate(R.layout.fragment_home, container, false);

        return view
    }

    override fun onStart() {
        super.onStart()
        pager.adapter = ViewPagerAdapter(this.childFragmentManager)
        tabs.setupWithViewPager(pager)
        tabs.getTabAt(0)?.text = this.getString(R.string.tab_all_labes)
        tabs.getTabAt(1)?.text = this.getString(R.string.tab_my_labes)
    }

    override fun onDataList(data: List<LabeModel>) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}