package com.banarum.labour.ui.menu.fragments.HotView

import android.util.Log
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.banarum.labour.app.LabourApp
import com.banarum.labour.network.models.LabeModel
import com.banarum.labour.network.models.LoginModel
import com.banarum.labour.network.services.LabourService
import rx.Observable
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import javax.inject.Inject

@InjectViewState
class HotPresenter : MvpPresenter<HotView> {


    @Inject
    lateinit var labourService:LabourService

    constructor() : super() {
        LabourApp.graph.inject(this)
    }

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
    }

    fun refreshLabes(category:String?){
        requestLabesData(category)
    }

    private fun requestLabesData(category: String?){
        val request = labourService.getLabesData(category)
        request.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map { responseObj -> responseObj?.data }
                .flatMap { labesData-> Observable.from(labesData) }
                .filter { labe -> labe.isHot }
                .toList()
                .onErrorReturn { listOf<LabeModel>() }
                .doOnError { e -> e.printStackTrace() }
                .subscribe{labesData -> viewState.onDataList(labesData!!)}
    }
}