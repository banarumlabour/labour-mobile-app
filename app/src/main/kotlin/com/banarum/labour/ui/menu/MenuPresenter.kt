package com.banarum.labour.ui.menu

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter


/**
 * Created by Sergey on 10.03.2017.
 */

@InjectViewState
class MenuPresenter: MvpPresenter<MenuView>() {

    var areFragmentsOnScreen = false

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
    }
}