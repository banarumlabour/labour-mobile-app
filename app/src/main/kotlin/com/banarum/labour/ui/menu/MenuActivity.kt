package com.banarum.labour.ui.menu

import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.banarum.labour.R
import com.banarum.labour.ui.menu.fragments.HomeView.HomeFragment
import com.banarum.labour.ui.menu.fragments.HotView.HotFragment
import com.banarum.labour.ui.menu.fragments.ProfileView.ProfileFragment
import kotlinx.android.synthetic.main.activity_menu.*


class MenuActivity : MvpAppCompatActivity(), MenuView {

    @JvmField val HOT_FRAGMENT_TAG = "HotFragment"
    @JvmField val HOME_FRAGMENT_TAG = "HomeFragment"
    @JvmField val PROFILE_FRAGMENT_TAG = "ProfileFragment"

    lateinit var hotFragment: HotFragment
    lateinit var homeFragment: HomeFragment
    lateinit var profileFragment: ProfileFragment

    @InjectPresenter
    lateinit var presenter: MenuPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)

        initFragments()

        bottomBar.setOnTabSelectListener { id ->
            when (id) {
                R.id.tab_hot -> onTabHot()
                R.id.tab_home -> onTabHome()
                R.id.tab_profile -> onTabProfile()
            }
        }
    }

    fun initFragments() {
        presenter.areFragmentsOnScreen = true

        /*
            after screen rotation android may save fragments and then restore
            we simply catching them instead of adding new ones
        */

        val savedHotFragment = supportFragmentManager.findFragmentByTag(HOT_FRAGMENT_TAG)
        val savedHomeFragment = supportFragmentManager.findFragmentByTag(HOME_FRAGMENT_TAG)
        val savedProfileFragment = supportFragmentManager.findFragmentByTag(PROFILE_FRAGMENT_TAG)

        if (savedHotFragment != null) {
            hotFragment = savedHotFragment as HotFragment
        }else{
            hotFragment = HotFragment()
            addFragment(hotFragment, HOT_FRAGMENT_TAG)
        }

        if (savedHomeFragment != null) {
            homeFragment = savedHomeFragment as HomeFragment
        }else{
            homeFragment = HomeFragment()
            addFragment(homeFragment, HOME_FRAGMENT_TAG)
        }

        if (savedProfileFragment != null) {
            profileFragment = savedProfileFragment as ProfileFragment
        }else{
            profileFragment = ProfileFragment()
            addFragment(profileFragment, PROFILE_FRAGMENT_TAG)
        }
    }

    fun onTabHot() {
        hideAllFragments()
        showFragment(hotFragment)
    }

    fun onTabHome() {
        hideAllFragments()
        showFragment(homeFragment)
    }

    fun onTabProfile() {
        hideAllFragments()
        showFragment(profileFragment)
    }

    private fun hideAllFragments() {
        if (!hotFragment.isHidden)
            hideFragment(hotFragment)

        if (!homeFragment.isHidden)
            hideFragment(homeFragment)

        if (!profileFragment.isHidden)
            hideFragment(profileFragment)
    }

    private fun addFragment(fragment: Fragment, tag: String) {
        supportFragmentManager.beginTransaction()
                .add(R.id.contentContainer, fragment, tag)
                .commit()
    }

    private fun showFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
                //.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out)
                .show(fragment)
                .commit()
    }

    private fun hideFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
                // .setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out)
                .hide(fragment)
                .commit()
    }
}