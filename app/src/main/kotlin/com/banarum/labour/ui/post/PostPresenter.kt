package com.banarum.labour.ui.post

import com.arellomobile.mvp.MvpPresenter
import com.banarum.labour.app.LabourApp
import com.banarum.labour.network.models.UserModel
import com.banarum.labour.network.services.LabourService
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import java.io.FileDescriptor
import javax.inject.Inject

class PostPresenter:MvpPresenter<PostView> {

    @Inject
    lateinit var labourService:LabourService

    constructor(){
        LabourApp.graph.inject(this)
    }

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
    }

    fun sendLabe(userModel: UserModel, title:String, description:String, price:Int, isHot:Boolean){
        val request = labourService.postLabe(userModel.id, title, description, price, isHot)
        request.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe{}
    }
}