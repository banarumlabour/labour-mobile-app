package com.banarum.labour.ui.login

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.banarum.labour.network.models.LoginModel
import com.banarum.labour.network.models.UserModel

@StateStrategyType(AddToEndSingleStrategy::class)
interface LoginView : MvpView {
    fun onLoginSucceed(userModel: UserModel)
    fun showContentView()
    fun showLoadingView()
    fun showToast(msg: String?)
}