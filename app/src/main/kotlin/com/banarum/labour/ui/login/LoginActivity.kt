package com.banarum.labour.ui.login

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.banarum.labour.R
import com.banarum.labour.app.LabourApp
import com.banarum.labour.di.subcomponent.LoginActivityModule
import com.banarum.labour.network.models.LoginModel
import com.banarum.labour.network.models.UserModel
import com.banarum.labour.ui.menu.MenuActivity
import com.banarum.labour.ui.menu.fragments.ProfileView.ProfileFragment
import kotlinx.android.synthetic.main.activity_login.*
import org.jetbrains.anko.onClick

class LoginActivity : MvpAppCompatActivity(), LoginView {

    companion object {
        @JvmField val PRM_LOGIN_MODEL = "LoginModel"
        @JvmField val PRM_USER_MODEL = "UserModel"
    }

    @InjectPresenter
    lateinit var presenter: LoginPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        LabourApp.graph.plus(LoginActivityModule(this)).inject(this)

        facebookButton.onClick {onFacebookClick()}

        vkButton.onClick {onVkClick()}
    }

    fun onFacebookClick(){
        presenter.facebookLogin(this)
        login_content.visibility = View.GONE
    }

    fun onVkClick(){
        presenter.vkLogin(this)

        //cause vk auth don't have any progress bar like in fb
        showLoadingView()
        //login_content.visibility = View.GONE
    }

    override fun showToast(msg: String?) {
        if (msg!=null)
            Toast.makeText(this, msg, Toast.LENGTH_LONG).show()
    }

    override fun showLoadingView() {
        progress_bar.visibility = View.VISIBLE
        login_content.visibility = View.GONE
    }

    override fun showContentView() {
        progress_bar.visibility = View.GONE
        login_content.visibility = View.VISIBLE
    }

    override fun onLoginSucceed(userModel: UserModel) {
        val intent = Intent(this, MenuActivity::class.java)
        intent.putExtra(PRM_USER_MODEL, userModel)
        startActivity(intent)
        finish()
        //showToast(loginModel.id + " " + loginModel.name + " " + loginModel.token)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        presenter.onActivityResult(requestCode, resultCode, data)
    }
}
