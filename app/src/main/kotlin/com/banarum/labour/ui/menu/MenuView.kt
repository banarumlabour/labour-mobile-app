package com.banarum.labour.ui.menu

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

@StateStrategyType(value= AddToEndSingleStrategy::class)
interface MenuView : MvpView{
}