package com.banarum.labour.ui.card

import android.os.Bundle
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.banarum.labour.R
import com.banarum.labour.common.transforms.CircleTransform
import com.banarum.labour.network.models.LabeModel
import com.banarum.labour.network.models.UserModel
import com.banarum.labour.ui.menu.fragments.HotView.HotFragment
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_card.*
import org.jetbrains.anko.onClick

class CardActivity : MvpAppCompatActivity(), CardView {

    @InjectPresenter
    lateinit var presenter: CardPresenter

    companion object {
        @JvmField val CARD_ACTIVITY = "labe"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_card)



    }

    override fun onStart() {
        super.onStart()

        val labeModel: LabeModel = intent.getParcelableExtra(HotFragment.CARD_ACTIVITY)
        presenter.getAuthorData(labeModel.userId)
        onData(labeModel)

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        card_join_button.onClick {
            Toast.makeText(this, resources.getString(R.string.joined), Toast.LENGTH_SHORT).show()
            finish()
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return true
    }



    fun setProfilePhoto(url: String) {
        Picasso.with(this)
                .load(url)
                .centerCrop()
                .transform(CircleTransform())
                .placeholder(R.drawable.profile_photo_placeholder)
                .fit()
                .into(profile_ico)
    }

    override fun onUserModel(userModel: UserModel) {
        setProfilePhoto(userModel.icoUrl)
        profile_user_name.text = userModel.name
    }

    fun onData (labeModel: LabeModel) {
        my_toolbar.title = labeModel.title
        card_description.text = labeModel.description
        card_date.text = labeModel.date.split(" ")[0]
        card_price.text = ((labeModel.price)/1000000).toString() + "₽"
        setSupportActionBar(my_toolbar)
    }

}