package com.banarum.labour.ui.menu.fragments.ProfileView

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.banarum.labour.app.LabourApp
import com.banarum.labour.network.models.UserModel
import com.banarum.labour.social.FacebookApi
import com.banarum.labour.social.VkApi
import com.banarum.labour.ui.login.LoginPresenter
import javax.inject.Inject

@InjectViewState
class ProfilePresenter : MvpPresenter<ProfileView> {

    @Inject
    lateinit var facebookApi:FacebookApi

    @Inject
    lateinit var vkApi: VkApi

    lateinit var userModel:UserModel

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
    }

    constructor() : super() {
        LabourApp.graph.inject(this)
    }

    fun onUserModel(userModel: UserModel){
        viewState.setUserData(userModel)
        this.userModel = userModel
    }

    fun logOut(){
        when (userModel.platform){
            LoginPresenter.FB_LOGIN_TYPE -> {
                facebookApi.logOut()
            }
            LoginPresenter.VK_LOGIN_TYPE -> {
                vkApi.logOut()
            }
        }

    }
}