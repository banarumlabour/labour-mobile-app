package com.banarum.labour.ui.post

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

@StateStrategyType(value= AddToEndSingleStrategy::class)
interface PostView : MvpView {
}