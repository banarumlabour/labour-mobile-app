package com.banarum.labour.ui.menu.fragments.HomeView.AllLabesView

import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.banarum.labour.R
import com.banarum.labour.network.models.LabeModel
import com.banarum.labour.ui.card.CardActivity
import com.banarum.labour.ui.menu.fragments.HomeView.HomeLabesAdapter
import com.banarum.labour.ui.menu.fragments.HomeView.HomePresenter
import com.banarum.labour.ui.menu.fragments.HotView.HotFragment
import kotlinx.android.synthetic.main.fragment_all_labes.*


class AllLabesFragment : MvpAppCompatFragment(), AllLabesView {

    @InjectPresenter
    lateinit var presenter: AllLabesPresenter

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater?.inflate(
                R.layout.fragment_all_labes, container, false) as ViewGroup

        return rootView
    }

    override fun onStart() {
        super.onStart()

        home_refresh_layout.setOnRefreshListener {
            onRefresh()
        }

        presenter.refreshLabes(null)
    }

    fun onRefresh() {
        presenter.refreshLabes(null)
    }

    override fun onDataList(data: List<LabeModel>) {
        val labesAdapter = HomeLabesAdapter()
        labesAdapter.data = data

        labesAdapter.setLabeClickAction { item ->
            //TODO действие по клику
            val intent = Intent(this.activity, CardActivity::class.java)
            intent.putExtra(HotFragment.CARD_ACTIVITY, item)
            this.activity.startActivity(intent)
        }

        home_recycler_view.layoutManager = LinearLayoutManager(this.activity!!)
        home_recycler_view.adapter = labesAdapter

        home_refresh_layout.isRefreshing = false
    }

}