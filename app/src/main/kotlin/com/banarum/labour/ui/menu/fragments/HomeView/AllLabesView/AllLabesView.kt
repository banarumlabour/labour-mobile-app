package com.banarum.labour.ui.menu.fragments.HomeView.AllLabesView

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.banarum.labour.network.models.LabeModel

@StateStrategyType(value = AddToEndSingleStrategy::class)
interface AllLabesView : MvpView {
    fun onDataList(data:List<LabeModel>)
}