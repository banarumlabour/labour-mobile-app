package com.banarum.labour.ui.menu.fragments.HotView

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.banarum.labour.network.models.LabeModel

@StateStrategyType(value = AddToEndSingleStrategy::class)
interface HotView : MvpView {
    fun onDataList(data:List<LabeModel>)
}