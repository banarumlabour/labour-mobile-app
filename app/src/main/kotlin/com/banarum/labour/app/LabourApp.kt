package com.banarum.labour.app

import android.app.Application
import com.banarum.labour.di.ApplicationComponent
import com.banarum.labour.di.ApplicationModule
import com.banarum.labour.di.DaggerApplicationComponent
import com.onesignal.OneSignal
import com.vk.sdk.VKSdk

class LabourApp : Application() {

    companion object {
        lateinit var graph: ApplicationComponent
    }

    override fun onCreate() {
        super.onCreate()
        graph = DaggerApplicationComponent.builder()
                .applicationModule(ApplicationModule(this))
                .build()

        OneSignal.startInit(this).init();

        VKSdk.initialize(this)
    }
}